from datetime import datetime
from gpiozero import Button
from picamera import PiCamera, Color
import os
from subprocess import check_call
from time import sleep

FOLDER_NAME = 'piself'
PIC_FILENAME = '{date}.jpg'
VID_FILENAME = '{date}.h264'

# uses BCM numeration:
KEY1_GPIO = 21
KEY2_GPIO = 20
KEY3_GPIO = 16
JOYUP_GPIO = 6
JOYDOWN_GPIO = 19
JOYLEFT_GPIO = 5
JOYRIGHT_GPIO = 26
JOYPRESS_GPIO = 13

class Hypermode:
    title = '--Hypermode--'
    def __init__(self, val):
        self.val = val
        pass

    def inc(self):
        pass

    def dec(self):
        pass
    
    def get_subtitle(self):
        pass


class Zoom(Hypermode):
    title = 'Zoom'
    name = 'zoom'

    def __init__(self, val, step=.05):
        self.val = val
        self.step = step
        self.zoom_list = list(val)

    def _calc(self, step):
        self.zoom_list
        self.zoom_list[:2] = [max(min(x + step, .4), 0) for x in self.zoom_list[:2]]
        self.zoom_list[2:] = [max(min(x - (2 * step), 1), .2) for x in self.zoom_list[2:]]
        self.val = tuple(self.zoom_list)
        return self.val

    def inc(self):
        return self._calc(step=self.step)
         
    def dec(self):
        return self._calc(step=-self.step)
    
    def get_subtitle(self):
        return str([round(x, 2) for x in self.val])


class ExposureMode(Hypermode):
    title = 'Exposure Mode'
    name = 'exposure_mode'
    modes = list(PiCamera.EXPOSURE_MODES.keys())
    def __init__(self, val):
        self.val = val
    
    def _calc(self, step):
        idx = max(min(self.modes.index(self.val) + step, len(self.modes) - 1), 0)
        return self.modes[idx]

    def inc(self):
        self.val = self._calc(step=1)
        return self.val
    
    def dec(self):
        self.val = self._calc(step=-1)
        return self.val

    def get_subtitle(self):
        return self.val


class MyCamera(PiCamera):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.static_annot = ''
        self.annotate_text_size = 70
        self.rotation = 180
        self.start_preview()

        self.key1 = Button(KEY1_GPIO)
        self.key2 = Button(KEY2_GPIO)
        self.key3 = Button(KEY3_GPIO, hold_time=5)
        self.joyup = Button(JOYUP_GPIO)
        self.joydown = Button(JOYDOWN_GPIO)
        self.joyleft = Button(JOYLEFT_GPIO)
        self.joyright = Button(JOYRIGHT_GPIO)
        self.joypress = Button(JOYPRESS_GPIO)
        
        self.display_idx = 0

        self.hypermodes = [ExposureMode(val=self.exposure_mode),
                           Zoom(val=self.zoom)]
        self.hypermode_idx = 0
        
        self.set_hypermode(idx=0)

        self.key3.when_pressed = self.announce_shutdown
        self.key3.when_released = self.abort_shutdown
        self.key3.when_held = lambda: check_call(['sudo', 'poweroff'])

        self.joyleft.when_pressed = lambda: self.set_hypermode(idx=max(self.hypermode_idx - 1, 0))
        self.joyright.when_pressed = lambda: self.set_hypermode(idx=min(self.hypermode_idx + 1, 1))
        self.joyup.when_pressed = lambda: self._set_attribute(name=self.hypermode.name,
                                                              val=self.hypermode.inc())
        self.joydown.when_pressed = lambda: self._set_attribute(name=self.hypermode.name,
                                                                val=self.hypermode.dec())
        self.key1.when_pressed = lambda: print(self.hypermode.val)

    def _set_attribute(self, name, val):
        if name == 'exposure_mode':
            self.exposure_mode = val
        elif name == 'zoom':
            self.zoom = val
        
        self.do_annotation()

    def announce_shutdown(self):
        print('hold 5 secs to power off')
        self.annotate_background = Color('red')
        for i in range(5):
            self.annotate_text = 'hold {} secs to power off'.format(i)
            sleep(1)
        

    def abort_shutdown(self):
        print('aborting shutdown')
        self.annotate_background = None
        self.do_annotation()

    def set_hypermode(self, idx):
        self.hypermode = self.hypermodes[idx]
        self.do_annotation()

    def do_annotation(self):
        self.annotate_text = '\n'.join([self.hypermode.title, self.hypermode.get_subtitle()])
    
if __name__ == '__main__':
    camera = MyCamera()
    while True:
        sleep(5)
